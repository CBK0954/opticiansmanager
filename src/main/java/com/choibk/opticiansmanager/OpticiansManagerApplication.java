package com.choibk.opticiansmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpticiansManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpticiansManagerApplication.class, args);
	}

}
