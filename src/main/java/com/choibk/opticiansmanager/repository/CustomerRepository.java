package com.choibk.opticiansmanager.repository;

import com.choibk.opticiansmanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
