package com.choibk.opticiansmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerItem {
    private Long id;
    private String customerName;
    private String customerPhone;
    private LocalDate dateLast;
}
