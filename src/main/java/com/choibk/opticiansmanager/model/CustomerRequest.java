package com.choibk.opticiansmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 11, max = 20)
    private String customerPhone;

    @NotNull
    private Float figureEyeRight;

    @NotNull
    private Float figureEyeLeft;
}
