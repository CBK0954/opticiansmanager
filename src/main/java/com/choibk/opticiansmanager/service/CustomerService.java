package com.choibk.opticiansmanager.service;

import com.choibk.opticiansmanager.entity.Customer;
import com.choibk.opticiansmanager.model.CustomerItem;
import com.choibk.opticiansmanager.model.CustomerRequest;
import com.choibk.opticiansmanager.model.CustomerResponse;
import com.choibk.opticiansmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setDateLast(LocalDate.now());
        addData.setFigureEyeRight(request.getFigureEyeRight());
        addData.setFigureEyeLeft(request.getFigureEyeLeft());

        customerRepository.save(addData);
    }

    public List<CustomerItem> getCustomers() {
        List<Customer> originList = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for(Customer item : originList) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerName(item.getCustomerName());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setDateLast(item.getDateLast());

            result.add(addItem);
        }

        return result;
    }

    public CustomerResponse getCustomer(long id) {
        Customer originData = customerRepository.findById(id).orElseThrow();

        CustomerResponse result = new CustomerResponse();
        result.setId(originData.getId());
        result.setCustomerName(originData.getCustomerName());
        result.setCustomerPhone(originData.getCustomerPhone());
        result.setDateLast(originData.getDateLast());
        result.setFigureEye(originData.getFigureEyeLeft() + "/" + originData.getFigureEyeRight());
        result.setNeedGlassesLeft(originData.getFigureEyeLeft() < 1.0 ? "안경필요" : "안경불필요");
        result.setNeedGlassesRight(originData.getFigureEyeRight() < 1.0 ? "안경필요" : "안경불필요");
        result.setIsSuperman((originData.getFigureEyeLeft() >= 2.0 && originData.getFigureEyeRight() >= 2.0) ? true : false);
        return result;
    }
}
